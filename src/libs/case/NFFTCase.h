#ifndef NFFTCASE_H
#define	NFFTCASE_H

#include "../../standardlibs.h"

class NFFTCase {

public:
	NFFTCase();
	~NFFTCase();

	// Attributes
	string aConfigFileName;
	string aFFTFile;
    string aBoundary2Skip;
    string aWindowFunction;

	vector<string> aSolnFile;
    vector<string> aFields4NFFT;
    vector<string> aSolnFields;


    int aMaxOptimIter;
    int aNoSolnFields;
    int aNoFields4NFFT;
	int aNfftMethod;
    int aDimension;
    int aColIndxPosition[3];
    vector<int> aNoFFTModes;

    double aInitValue;
    vector<vector<double> > aSoln;
    vector<vector<double> > aSolnFFT_real;
    vector<vector<double> > aSolnFFT_imag;

	// Methods
	int readConfigFile();
    int readSolutionFile_CSV();
    
    int writeFFTSolutionFile();
    int writeFFTSolutionFile_ASCII();

    int readFFTSolutionFile(){return 0;};
    int writeSolutionFile(){return 0;};

    int doNFFTonFields();
    int doNFFT(vector<double>& pSoln, vector<double>& pSolnFFT_real, vector<double>& pSolnFFT_imag);

    int doWindowOnFields();
    int doWindow(vector<double>& pSoln, string pWindowName);
    int doHanningWindow(vector<double>& pSoln);

};

#endif
