#include "parser.h"

/** Parse for a pKeyWord and read the specified value into a string variable
 */
string parser_read_string(istream& pConfigFile, string pKeyWord, string pDefaultValue) {
    string val = pDefaultValue;


    string lineRead="";
    int iLine = 0;

    // Clear the error state flag
    pConfigFile.clear();
    // reseting the file position to the begining of the file
    pConfigFile.seekg(0, pConfigFile.beg);

    /********************************************/

    // While not end of file
    while (!pConfigFile.eof() ){
        std::getline(pConfigFile, lineRead);
        iLine++;

        lineRead = trim2(lineRead);

        size_t lFound = lineRead.find(pKeyWord);
        if (lFound != string::npos) {
            size_t pos = lineRead.rfind("=");
            if (pos == string::npos) {
                cout << " No equal sign in line # " << iLine << endl;
                cout << " Check your config file." << endl;
            } else {
                string sval = lineRead.substr(pos + 1);
                sval = trim2(sval);
                std::stringstream ss(sval);
                val = ss.str();
            }//endif

        }
    } //end while

    cout << "     - " << pKeyWord << " = " <<  val << endl;

    return trim2(val);
}

/** Parse for a pKeyWord and read the specified value into a double variable
 */
double parser_read_double(istream& pConfigFile, string pKeyWord, double pDefaultValue) {

    double val = pDefaultValue;

    string lineRead;
    int iLine = 0;

    /* reseting the file position to the begining of the file*/
    pConfigFile.clear();
    pConfigFile.seekg(0, pConfigFile.beg);

    /********************************************/

    while (!pConfigFile.eof()) //while not EOF
    {
        std::getline(pConfigFile, lineRead);
        iLine++;

        lineRead = trim2(lineRead);


        size_t lFound = lineRead.find(pKeyWord);
        if (lFound != string::npos) {
            size_t pos = lineRead.rfind("=");
            if (pos == string::npos) {
                cout << " No equal sign in line # " << iLine << endl;
                cout << " Check your config file." << endl;
            } else {
                string sval = lineRead.substr(pos + 1);
                trim2(sval);
                std::stringstream ss(sval);
                ss >> val;
            }//endif

        }
    } //end while

    cout << "     - " << pKeyWord << " = " <<  val << endl;

    return val;
}

/** Parse for a pKeyWord and read the specified value into an integer variable
 */
int parser_read_int(istream& pConfigFile, string pKeyWord, int pDefaultValue) {
    int val = pDefaultValue;

    string lineRead;
    int iLine = 0;

    /* reseting the file position to the begining of the file*/
    pConfigFile.clear();
    pConfigFile.seekg(0, pConfigFile.beg);

    /********************************************/

    while (!pConfigFile.eof()) //while not EOF
    {
        std::getline(pConfigFile, lineRead);
        iLine++;

        lineRead = trim2(lineRead);


        size_t lFound = lineRead.find(pKeyWord);
        if (lFound != string::npos) {
            size_t pos = lineRead.rfind("=");
            if (pos == string::npos) {
                cout << " No equal sign in line # " << iLine << endl;
                cout << " Check your config file." << endl;
            } else {
                string sval = lineRead.substr(pos + 1);
                trim2(sval);
                std::stringstream ss(sval);
                ss >> val;
            }//endif

        }
    } //end while

    cout << "     - " << pKeyWord << " = " <<  val << endl;
    return val;
}

void parser_array_int(istream& pConfigFile, string pKeyWord, vector<int>& pValues) {

    string lineRead;
    int iLine = 0;

    /* reseting the file position to the begining of the file*/
    pConfigFile.clear();
    pConfigFile.seekg(0, pConfigFile.beg);

    /********************************************/
    //while not EOF, read lines
    while (!pConfigFile.eof()){

        // Read a line
        std::getline(pConfigFile, lineRead);
        iLine++;

        // Trim the line
        lineRead = trim2(lineRead);

        // Find the opening and closing parantheses
        size_t lFound = lineRead.find(pKeyWord);
        if (lFound != string::npos) {
            size_t pos_start = lineRead.rfind("(");
            if (pos_start == string::npos) {
                cout << " No parantheses qual in line # " << iLine << endl;
                cout << " When specifying an array, you should always put it as (x,y,z,...,) even if it is a single number" << endl;
                cout << " Check your config file." << endl;
                die("ERR in parse_array_int: the array is not formatted correctly. Missing = or : signs.");
            } else {
                size_t pos_end = lineRead.rfind(")");
                string t1 = lineRead;
                if (pos_end == string::npos)
                    while (pos_end == string::npos) {
                        // if the closing parantheses is not present
                        cout << " Closing parantheses is missing. Make sure it is on the same line" << endl;
                        die("ERR in parse_array_int: the array is not formatted correctly. Missing = or : signs.");
                    }

                size_t t1_start = pos_start;
                size_t t1_end = pos_end;

                size_t length = t1_end - t1_start;
                string t2 = t1.substr(t1_start + 1, length-1);

                // Find the position of first camma
                size_t pos_comma = t2.find(",");
                while ( (pos_comma != string::npos) ) {

                    size_t len_val = pos_comma;

                    string sval = trim2(t2.substr(0,len_val));
                    std::stringstream ss(sval);
                    int val = 0;
                    ss >> val;

                    pValues.push_back(val);

                    // Cut the string short
                    t2 = t2.substr(pos_comma+1,t2.length());

                    // Find the location of next comma
                    pos_comma = t2.find(",");
                }
                // Extract the last number, drop parantheses
                string sval = trim2(t2.substr(0,t2.length()));
                std::stringstream ss(sval);
                int val = 0;
                ss >> val;
                pValues.push_back(val);
            }//endif
        }
    } //end while


    cout << "     - " << pKeyWord << " = ";
    for ( int i = 0; i < pValues.size(); i++ ){
        cout << pValues[i] << ",";
    }
    cout << endl;

}

/** Parse for pKeyWord and read it into an array of strings
 */
void parser_array_string(istream& pConfigFile, string pKeyWord, vector<string>& pValues) {

    string lineRead;
    int iLine = 0;

    /* reseting the file position to the begining of the file*/
    pConfigFile.clear();
    pConfigFile.seekg(0, pConfigFile.beg);

    /********************************************/
    //while not EOF, read lines
    while (!pConfigFile.eof()){

        // Read a line
        std::getline(pConfigFile, lineRead);
        iLine++;

        // Trim the line
        lineRead = trim2(lineRead);

        // Find the opening and closing parantheses
        size_t lFound = lineRead.find(pKeyWord);
        if (lFound != string::npos) {
            size_t pos_start = lineRead.rfind("(");
            if (pos_start == string::npos) {
                cout << " No parantheses qual in line # " << iLine << endl;
                cout << " When specifying an array, you should always put it as (x,y,z,...,) even if it is a single number" << endl;
                cout << " Check your config file." << endl;
                die("ERR in parse_array_string: the array is not formatted correctly. Missing = or : signs.");
            } else {
                size_t pos_end = lineRead.rfind(")");
                string t1 = lineRead;
                if (pos_end == string::npos)
                    while (pos_end == string::npos) {
                        // if the closing parantheses is not present
                        cout << " Closing parantheses is missing. Make sure it is on the same line" << endl;
                        die("ERR in parse_array_string: the array is not formatted correctly. Missing = or : signs.");
                    }

                size_t t1_start = pos_start;
                size_t t1_end = pos_end;

                size_t length = t1_end - t1_start;
                string t2 = t1.substr(t1_start + 1, length-1);

                // Find the position of first camma
                size_t pos_comma = t2.find(",");
                while ( (pos_comma != string::npos) ) {

                    size_t len_val = pos_comma;

                    string sval = trim2(t2.substr(0,len_val));
                    pValues.push_back(sval);

                    // Cut the string short
                    t2 = t2.substr(pos_comma+1,t2.length());

                    // Find the location of next comma
                    pos_comma = t2.find(",");
                }
                // Extract the last number, drop parantheses
                string sval = trim2(t2.substr(0,t2.length()));
                pValues.push_back(sval);
            }//endif
        }
    } //end while


    cout << "     - " << pKeyWord << " = ";
    for ( int i = 0; i < pValues.size(); i++ ){
        cout << pValues[i] << ",";
    }
    cout << endl;

}

/** Parse a given string for an array of strings in a comma separated format
 */
void parser_array_string( string pString, vector<string>& pStringsArray ){

    // Find the position of first camma
    string lStringTemp = pString;
    size_t pos_comma = lStringTemp.find(",");
    while ( (pos_comma != string::npos) ) {

        size_t len_val = pos_comma;

        string sval = trim2(lStringTemp.substr(0,len_val));
        pStringsArray.push_back(sval);

        // Cut the string short
        lStringTemp = lStringTemp.substr(pos_comma+1,lStringTemp.length());

        // Find the location of next comma
        pos_comma = lStringTemp.find(",");
    }
    // Extract the last number, drop parantheses
    string sval = trim2(lStringTemp.substr(0,lStringTemp.length()));
    pStringsArray.push_back(sval);



}

/** Parse a given string for an array of integers in a comma separated format
 */
void parser_array_int( string pString, vector<int>& pIntArray ){

    // Find the position of first camma
    string lStringTemp = pString;
    size_t pos_comma = lStringTemp.find(",");
    while ( (pos_comma != string::npos) ) {

        size_t len_val = pos_comma;

        string sval = trim2(lStringTemp.substr(0,len_val));
        int val = 0;
        std::stringstream ss(sval);
        ss >> val;
        pIntArray.push_back(val);

        // Cut the string short
        lStringTemp = lStringTemp.substr(pos_comma+1,lStringTemp.length());

        // Find the location of next comma
        pos_comma = lStringTemp.find(",");
    }
    // Extract the last number, drop parantheses
    string sval = trim2(lStringTemp.substr(0,lStringTemp.length()));
    int val = 0;
    std::stringstream ss(sval);
    ss >> val;
    pIntArray.push_back(val);

}

/** Parse a given string for an array of doubles in a comma separated format
 */
void parser_array_double( string pString, vector<double>& pDoubleArray ){

    // Find the position of first camma
    string lStringTemp = pString;
    size_t pos_comma = lStringTemp.find(",");
    while ( (pos_comma != string::npos) ) {

        size_t len_val = pos_comma;

        string sval = trim2(lStringTemp.substr(0,len_val));
        double val = 0.0;
        std::stringstream ss(sval);
        ss >> val;
        pDoubleArray.push_back(val);

        // Cut the string short
        lStringTemp = lStringTemp.substr(pos_comma+1,lStringTemp.length());

        // Find the location of next comma
        pos_comma = lStringTemp.find(",");
    }
    // Extract the last number, drop parantheses
    string sval = trim2(lStringTemp.substr(0,lStringTemp.length()));
    double val = 0.0;
    std::stringstream ss(sval);
    ss >> val;
    pDoubleArray.push_back(val);

}


string trim2(string str) {

    string::size_type pos = str.find_last_not_of(' ');
    if (pos != string::npos) {
        str.erase(pos + 1);
        pos = str.find_first_not_of(' ');
        if (pos != string::npos) str.erase(0, pos);
    } else str.erase(str.begin(), str.end());

    string::size_type pos1 = str.find('#');
    if (pos1 != string::npos) {
        str.erase(pos1);
    }

    string::size_type pos2 = str.find('!');
    if (pos2 != string::npos) {
        str.erase(pos2);
    }

    return str;
}

void die(string msg) {
    cout << msg << endl;
    exit(EXIT_FAILURE);
}



