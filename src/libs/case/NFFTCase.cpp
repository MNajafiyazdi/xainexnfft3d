#include "NFFTCase.h"
#include "parser.h"
#include <complex.h>"

#define NFFT_PRECISION_DOUBLE

#ifdef HAVE_COMPLEX_H
#include <complex.h>
#endif

#include "nfft3mp.h"

const double epsilon = 1.0e-12;


/** Empty decleration of the NFFTCase class
*/
NFFTCase::NFFTCase(){
	this->aConfigFileName = "config.nfft";
	this->aSolnFile.push_back("solution.csv");
	this->aFFTFile = "fft_solution.vts";
	this->aNfftMethod = Backward_NFFT;
    this->aDimension = 1;
    this->aNoSolnFields = 0;
    this->aNoFields4NFFT = 0;
    this->aColIndxPosition[0] = -1;
    this->aColIndxPosition[1] = -1;
    this->aColIndxPosition[2] = -1;
    this->aMaxOptimIter = 50;
    this->aBoundary2Skip = "";
    this->aInitValue = 0.0;
    this->aWindowFunction = "";
}

/** Destructor for NFFTCase class
*/
NFFTCase::~NFFTCase(){
}

/** Reads the config file specified by aConfigFileName
*/
int NFFTCase::readConfigFile(){

	cout << " ----- Reading Config File ... " << this->aConfigFileName << endl;
	int lReadStatus = 0;

	// Check if the file exists
	ifstream lConfigFile;
	lConfigFile.open(this->aConfigFileName.c_str(), ios::in);

	if ( !lConfigFile ){
		cerr << "   !!! Error: Config File doesn't exist" << endl;
		lReadStatus = -100;
		return lReadStatus;
    }

    // Parse the file for each of the options
    vector<string> lFileNames;
    parser_array_string(lConfigFile, "solution file", lFileNames);
    this->aSolnFile.resize(lFileNames.size());
    for ( int i = 0; i < lFileNames.size(); i++ )
        this->aSolnFile[i] = lFileNames[i];

    this->aFFTFile = parser_read_string(lConfigFile, "fft file", "fft_solution.vtu");
    string lNfftMethod = parser_read_string(lConfigFile, "fft operation", "Backward");
    if ( lNfftMethod.compare("Backward") )
        this->aNfftMethod = Backward_NFFT;
    else
        this->aNfftMethod = Forward_NFFT;

    this->aDimension = parser_read_int(lConfigFile, "fft dimension", 1);
    parser_array_int(lConfigFile, "no. fft modes", this->aNoFFTModes );

    parser_array_string(lConfigFile, "solution fields", this->aFields4NFFT);
    this->aNoFields4NFFT = this->aFields4NFFT.size();

    this->aMaxOptimIter = parser_read_int(lConfigFile, "maximum iterations",50);

    this->aBoundary2Skip = parser_read_string(lConfigFile, "skip boundary", "");

    this->aInitValue = parser_read_double(lConfigFile, "initial guess", 0.0);

    this->aWindowFunction = parser_read_string(lConfigFile, "window function", "");


    lConfigFile.close();
	cout << " ----- Reading Config File : Successful! " << endl;
	return lReadStatus;
}

/** Read the solution file specified by aSolnFile in a CSV format with header
 *  It can read a series of files
 */
int NFFTCase::readSolutionFile_CSV(){
    int lReadStatus = 0;

    cout << " ----- Reading Solution Files ... " << endl;

    for ( int is = 0; is < this->aSolnFile.size(); is++ ){

        cout << "    Solution File = " << this->aSolnFile[is] << endl;

        // Read the solution file specified by aSolnFile
        ifstream lSolnFile;
        lSolnFile.open(this->aSolnFile[is].c_str(), ios::in);

        if ( !lSolnFile ){
            cerr << "  !!! Error: Solution File doesn't exist" << endl;
            lReadStatus = -100;
            return lReadStatus;
        }

        // Reda the solution file header
        int lLineNo = 0;
        string lineHeader;
        std::getline(lSolnFile, lineHeader);
        lLineNo++;

        // Get the current size of the solution
        int lCurSize = this->aSoln.size();

        if ( lCurSize == 0 ){
            parser_array_string( lineHeader, this->aSolnFields);

            for ( int i = 0; i < this->aSolnFields.size(); i++ ){
                cout << "     Field[" << i << "] = " << this->aSolnFields[i] << endl;
            }
        }

        // Size the aSoln for parameters
        int lNoParameters = this->aSolnFields.size();
 
        if ( lCurSize == 0 ){
            this->aSoln.resize(lNoParameters);
            this->aNoSolnFields = lNoParameters;
        }

        // Count the number of nodes (as the number of lines)
        int lNbNodes = 0;
        string lStringTemp;
        while(std::getline(lSolnFile, lStringTemp))
            lNbNodes++;

        // Current number of nodes
        int lCurNbNodes = this->aSoln[0].size();

        // Resize the solution vectors
        for ( int i = 0; i < lNoParameters; i++ ){
            this->aSoln[i].resize(lCurNbNodes + lNbNodes);
            // Record the column index related to the positions
            if( this->aSolnFields[i] == "\"Points:0\""){
                this->aColIndxPosition[0] = i;
            }else if ( this->aSolnFields[i] == "\"Points:1\""){
                this->aColIndxPosition[1] = i;
            }else if ( this->aSolnFields[i] == "\"Points:2\""){
                this->aColIndxPosition[2] = i;
            }
        }

        // Reset the reader to the beginning of the file
        lSolnFile.clear(); // clear faild and eof bits
        lSolnFile.seekg(0, std::ios::beg); //back to the start!

        // read a dummy line (header)
        std::getline(lSolnFile, lStringTemp);

        // Read the solution fields 
        for ( int i = 0; i < lNbNodes; i++ ){
            // read each line
            getline(lSolnFile, lStringTemp);
            vector<double> lDoubleArray;
            // parse the line into an array of doubles
            parser_array_double(lStringTemp, lDoubleArray);
            for ( int ip = 0; ip < lNoParameters; ip++ ){
                this->aSoln[ip][lCurNbNodes + i] = lDoubleArray[ip];
            }
        }

        // Close the file
        lSolnFile.close();
    }
    cout << " ----- Reading Solution Files : Successful! " << endl;
    return lReadStatus = 0;

}

/** Perform windowing on the selected fields of the solution
 */
int NFFTCase::doWindowOnFields(){

    int lReturnStatus = 0;
    cout << " ----- Performing Windowing on Original Data ... " << endl;

    // By default, assume a set of fields is given
    bool ldoAllFields = false;

    // Check if it is so. If not, Windowing should be performed for all
    if ( this->aNoFields4NFFT == 0 && this->aFields4NFFT.empty() )
        ldoAllFields = true;


    vector<int> lSolnIndex;

    if ( ldoAllFields ){
        // If all fields should be used for Windowing , prepare the index list for all
        lSolnIndex.resize(this->aNoSolnFields);
        for ( int ifield = 0; ifield < this->aNoSolnFields; ifield++ )
            lSolnIndex[ifield] = ifield;

    }else{
        // If only a given set of fields should be used for Windowing, find the 
        // index related to them

        for ( int ifield = 0; ifield < this->aNoSolnFields; ifield++ ){
            for ( int i = 0; i < this->aNoFields4NFFT; i++ ){
                if ( this->aFields4NFFT[i] == this->aSolnFields[ifield] ){
                    lSolnIndex.push_back(ifield);
                }
            }
        }

    }

    // Apply the Window
    for ( int i = 0; i < lSolnIndex.size(); i++ ){
        cout << "     - Performing Windowing on : " << this->aSolnFields[lSolnIndex[i]] << " ... ";
        int lNFFTStatus = this->doWindow(this->aSoln[lSolnIndex[i]], this->aWindowFunction);
        if ( lNFFTStatus == 0 )
            cout << "     - Windowing on : " << this->aSolnFields[lSolnIndex[i]] << " ... Successful!" << endl;
        else
            cout << "     - Windowing on : " << this->aSolnFields[lSolnIndex[i]] << " ... Failed!" << endl;
    }



    cout << " ----- Performing Windowing on Original Data : Successful! " << endl;
    return lReturnStatus;

}

/** Perform windowing on the given solution set
 */
int NFFTCase::doWindow(vector<double>& pSoln, string pWindowName ){

    int lReturnStatus = 0;

    if ( pWindowName == "Hanning" ){
        lReturnStatus = doHanningWindow(pSoln);
    }

    return lReturnStatus;

}

/** Apply Hanning window on the solution
 */
int NFFTCase::doHanningWindow(vector<double>& pSoln){

    int lReturnStatus = 0;
    cout << " :: Hanning Window ... ";

    int lDim = this->aDimension;

    // Find min, and max coordinates for each dimension
    vector<double> lMinCoords(lDim,1.0e10);
    vector<double> lMaxCoords(lDim,-1.0e10);

    for ( int in = 0; in < pSoln.size(); in++ ){
        for ( int id = 0; id < lDim; id++){
            lMaxCoords[id] = max(lMaxCoords[id], this->aSoln[this->aColIndxPosition[id]][in]);
            lMinCoords[id] = min(lMinCoords[id], this->aSoln[this->aColIndxPosition[id]][in]);
        }
    }

    double pi = 3.14159265359;

    // Apply an n-dimensional Hnning Window
    for ( int in = 0; in < pSoln.size(); in++ ){
        for ( int id = 0; id < lDim; id++ ){
            double X = this->aSoln[this->aColIndxPosition[id]][in];
            double X_mid = 0.5 * ( lMaxCoords[id] + lMinCoords[id] );
            pSoln[in] *= 0.5 * ( 1 - cos( 2 * pi * X / (lMaxCoords[id])-lMinCoords[id]) );
        }
    }

    cout << " : Done! " << endl;
    return lReturnStatus;
}

/** Perform NFFT on the given solution, aSoln. If a set of fields are specified, aFields4NFFT, then
 *  the NFFT is performed only on them, otherwise, it is performed on all of the fields
 */
int NFFTCase::doNFFTonFields(){

    int lReturnStatus = 0;
    cout << " ----- Performing NFFT ... " << endl;

    // By default, assume a set of fields is given
    bool ldoAllFields = false;

    // Check if it is so. If not, NFFT should be performed for all
    if ( this->aNoFields4NFFT == 0 && this->aFields4NFFT.empty() )
        ldoAllFields = true;


    vector<int> lSolnIndex;

    if ( ldoAllFields ){
        // If all fields should be used for NFFT, prepare the index list for all
        lSolnIndex.resize(this->aNoSolnFields);
        for ( int ifield = 0; ifield < this->aNoSolnFields; ifield++ )
            lSolnIndex[ifield] = ifield;

    }else{
        // If only a given set of fields should be used for NFFT, find the 
        // index related to them

        for ( int ifield = 0; ifield < this->aNoSolnFields; ifield++ ){
            for ( int i = 0; i < this->aNoFields4NFFT; i++ ){
                if ( this->aFields4NFFT[i] == this->aSolnFields[ifield] ){
                    lSolnIndex.push_back(ifield);
                }
            }
        }

    }

    // Prepare for storing the NFFT results
    this->aSolnFFT_real.resize(lSolnIndex.size());
    this->aSolnFFT_imag.resize(lSolnIndex.size());

    // Perform NFFT on the list of fields
    for ( int i = 0; i < lSolnIndex.size(); i++ ){
        cout << "     - Performing NFFT on : " << this->aSolnFields[lSolnIndex[i]] << " ... ";
        int lNFFTStatus = this->doNFFT(this->aSoln[lSolnIndex[i]], this->aSolnFFT_real[i], this->aSolnFFT_imag[i]);
        if ( lNFFTStatus == 0 )
            cout << "     - NFFT on : " << this->aSolnFields[lSolnIndex[i]] << " ... Successful!" << endl;
        else
            cout << "     - NFFT on : " << this->aSolnFields[lSolnIndex[i]] << " ... Failed!" << endl;
    }

    cout << " ----- Performing NFFT : Successful! " << endl;
    return lReturnStatus;

}

/** Adopted from glacier.c, an example case from NFFT package
 */
static NFFT_R my_weight(NFFT_R pZ, NFFT_R pA, NFFT_R pB, NFFT_R pC){
    return NFFT_M(pow)(NFFT_K(0.25) - pZ * pZ, pB) / (pC + NFFT_M(pow)(NFFT_M(fabs)(pZ),NFFT_K(2.0)*pA));
}

ios::fmtflags f(cout.flags());

/** Performe NFFT with cross validation
 */
int NFFTCase::doNFFT(vector<double>& pSoln, vector<double>& pSolnFFT_real, vector<double>& pSolnFFT_imag){

    int lReturnStatus = 0;

    if ( this->aDimension < 1 || this->aDimension > 3 ){
        cout << " Error: XainexNFFT can only run in 1,2, or 3 dimensions" << endl;
        die(" xxxxxxxxxxxxxxxxx " );
    }


    // Prepare for NFFT
    int lDim = this->aDimension;

    cout << " :: " << lDim << " NFFT ... " << endl;
    
    // Main NFFT Plan
    NFFT(plan) lMyPlan;

    // Main Inverse NFFT Plan
    SOLVER(plan_complex) lMyPlanInverse;

    // Number of nodes in the solution
    int lNoNodes = pSoln.size();
    
    // Specifying the oversampling size
    vector<int> lN_OverSampled(this->aDimension);
    for ( int i = 0; i < this->aDimension; i++ ){
        lN_OverSampled[i] = (int)NFFT(next_power_of_2)(this->aNoFFTModes[i]+1);
        cout << "       - Oversampling[" << i << "] = " << lN_OverSampled[i] << endl;
    }

    // Get max & min to scale the coordinates such that x = [-0.5,0.5)
    vector<double> lXMin(lDim,1.0e10);
    vector<double> lXMax(lDim,-1.0e10);
    vector<double> lX(lDim,0.0);

    for ( int i = 0; i < lNoNodes; i++){
        // Get the max and min values of nodal coordinates
        for ( int j = 0; j < lDim; j++ ) {
            lX[j] = this->aSoln[this->aColIndxPosition[j]][i];
            if ( lX[j] > lXMax[j] )
                lXMax[j] = lX[j];
            else if ( lX[j] < lXMin[j] )
                lXMin[j] = lX[j];
        }
    }

    // Number of nodes to be used for NFFT
    int lNoNodes4NFFT = 0;
    vector<int> lNodes4NFFT;

    // Check which boundary should be skipped
    if ( this->aBoundary2Skip == "" ){

        // Nothing is skipped
        lNoNodes4NFFT = lNoNodes;
        lNodes4NFFT.resize(lNoNodes);
        for ( int i = 0; i < lNoNodes; i++ )
            lNodes4NFFT[i] = i;

    } else if ( this->aBoundary2Skip == "max" ){

        cout << " >>>> Skipping max boundary in each direction ..... " << endl;

        for ( int i = 0; i < lNoNodes; i++ ){
            bool lAddNode4NFFT = true;
            for ( int id = 0; id < lDim; id++ ){
                double lX = this->aSoln[this->aColIndxPosition[id]][i];
                if ( fabs(lX-lXMax[id]) < epsilon )
                    lAddNode4NFFT = false;
            }
            if ( lAddNode4NFFT ){
                lNodes4NFFT.push_back(i);
                lNoNodes4NFFT++;
            }
        }
        cout << " No. Nodes = " << lNoNodes << "  No. Nodes 4 NFFT = " << lNoNodes4NFFT << endl;

    } else if ( this->aBoundary2Skip == "min" ){

        for ( int i = 0; i < lNoNodes; i++ ){
            bool lAddNode4NFFT = true;
            for ( int id = 0; id < lDim; id++ ){
                double lX = this->aSoln[this->aColIndxPosition[id]][i];
                if ( fabs(lX-lXMin[id]) < epsilon )
                    lAddNode4NFFT = false;
            }

            if ( lAddNode4NFFT ){
                lNodes4NFFT.push_back(lNoNodes4NFFT);
                lNoNodes4NFFT++;
            }

        }
        cout << " No. Nodes = " << lNoNodes << "  No. Nodes 4 NFFT = " << lNoNodes4NFFT << endl;

    }


    // The cut-off parameter for NFFT
    int lCutOffParam = 2*lDim;

    // Initialize the plan for reconstruction
    NFFT(init_guru)(&lMyPlan, lDim, &this->aNoFFTModes[0], lNoNodes4NFFT, &lN_OverSampled[0], lCutOffParam,  
                    PRE_PHI_HUT| PRE_PSI| MALLOC_F_HAT| MALLOC_X| MALLOC_F | FFTW_INIT | 
                    FFT_OUT_OF_PLACE, FFTW_ESTIMATE| FFTW_DESTROY_INPUT);


    // Initialize the inverse plan, minimizing the error = CGNE 
    SOLVER(init_advanced_complex)(&lMyPlanInverse, (NFFT(mv_plan_complex)*) (&lMyPlan), CGNR | PRECOMPUTE_DAMP );


    // Read the sampling locations into the NFFT Plan & the signal values into the Inverse Plan
    for ( int i = 0; i < lNoNodes4NFFT; i++){
        // Simply initialize the values for y from the solution
        lMyPlanInverse.y[i] = (NFFT_R(pSoln[lNodes4NFFT[i]]));
    }

    // Scale the nodal coordinates into [-0.5, 0.5)
    for ( int i = 0; i < lNoNodes4NFFT; i++ ){
        int indx = lNodes4NFFT[i];
        for ( int j = 0; j < lDim; j++ ){
            lX[j] = this->aSoln[this->aColIndxPosition[j]][indx];
            // Set the normalized coordinates in [-0.5, 0.5)
            lMyPlan.x[lDim*i+j] = (lX[j] - lXMin[j]) / (lXMax[j] - lXMin[j]) - 0.5;
        }
    }

    // Precompute the nfft matrices if needed
    if ( lMyPlan.flags & PRE_ONE_PSI )
        NFFT(precompute_one_psi)(&lMyPlan);

    // Initialize damping factor
    if ( lMyPlanInverse.flags & PRECOMPUTE_DAMP ){

        if ( lDim == 2 ){ 
            for ( int k0 = 0; k0 < lMyPlan.N[0]; k0++ )
                for ( int k1 = 0; k1 < lMyPlan.N[1]; k1++ )
                    lMyPlanInverse.w_hat[k0*lMyPlan.N[1] + k1] = my_weight(((NFFT_R) ((NFFT_R)(k0) - (NFFT_R)(lMyPlan.N[0]) / NFFT_K(2.0))) / ((NFFT_R)(lMyPlan.N[0])),
                            NFFT_K(0.5), NFFT_K(3.0), NFFT_K(0.001)) * my_weight((((NFFT_R)(k1) - (NFFT_R)(lMyPlan.N[1]) / NFFT_K(2.0))) / ((NFFT_R)(lMyPlan.N[1])), 
                            NFFT_K(0.5), NFFT_K(3.0), NFFT_K(0.001));
        }else if ( lDim == 3 ) {
            for ( int k0 = 0; k0 < lMyPlan.N[0]; k0++ )
                for ( int k1 = 0; k1 < lMyPlan.N[1]; k1++ )
                    for ( int k2 = 0; k2 < lMyPlan.N[2]; k2++ )
                        lMyPlanInverse.w_hat[k0*lMyPlan.N[2]*lMyPlan.N[1] + k1*lMyPlan.N[2] + k2] =
                                my_weight(((NFFT_R) ((NFFT_R)(k0) - (NFFT_R)(lMyPlan.N[0]) / NFFT_K(2.0))) / ((NFFT_R)(lMyPlan.N[0])), NFFT_K(0.5), NFFT_K(3.0), NFFT_K(0.001)) * 
                                my_weight(((NFFT_R) ((NFFT_R)(k1) - (NFFT_R)(lMyPlan.N[1]) / NFFT_K(2.0))) / ((NFFT_R)(lMyPlan.N[1])), NFFT_K(0.5), NFFT_K(3.0), NFFT_K(0.001)) *
                                my_weight(((NFFT_R) ((NFFT_R)(k2) - (NFFT_R)(lMyPlan.N[2]) / NFFT_K(2.0))) / ((NFFT_R)(lMyPlan.N[2])), NFFT_K(0.5), NFFT_K(3.0), NFFT_K(0.001)); 
        }
    }

    // Initial guess is f_hat = 0 for all wavenumbers
    cout << "     - Initial guess = " << this->aInitValue << endl;
    for ( int k = 0; k < lMyPlan.N_total; k++ ){
        lMyPlanInverse.f_hat_iter[k] = NFFT_K(this->aInitValue);
    }

    // Write down the inital guess
//    NFFT(vpr_complex)(lMyPlanInverse.f_hat_iter, lMyPlan.N_total, "Initial Guess, vector f_hat_iter")
//

    // Check for valid parameters before calling any trafo/adjoint method
    const char* lErr_Status = NFFT(check)(&lMyPlan);
    if ( lErr_Status != 0 ){
        cout << " ERROR: Error in NFFT module: " << lErr_Status << endl;
        die(" xxxxxxxxxxxxxxxx ");
    }

    // Inverse transformation
    SOLVER(before_loop_complex)(&lMyPlanInverse);

    // Perform the optimization to find the correct inverse transform
    for ( int lIter = 0; lIter < this->aMaxOptimIter; lIter++ ){

            
            double lResidual = NFFT_M(sqrt)(lMyPlanInverse.dot_r_iter);
            for ( int id = 0; id < this->aDimension; id++ )
                    lResidual /= double(lN_OverSampled[id]);

            cout << "      " << setw(4) << lIter <<  " Residual ||r|| = ";
            cout.flags(f);
            // cout << NFFT_M(sqrt)(lMyPlanInverse.dot_r_iter) << endl;
            cout << lResidual << endl;

            // If enough convergance, simply finish
            // if ( NFFT_M(sqrt)(lMyPlanInverse.dot_r_iter) < 1.0e-12 ) 
            //    break;
            if ( lResidual < 1.0e-16 )
                break;
        // Another loop of solver
        SOLVER(loop_one_step_complex)(&lMyPlanInverse);

        // Solution fitting
//        NFFT_CSWAP(lMyPlanInverse.f_hat_iter,lMyPlan.f_hat);
//        NFFT(trafo)(&lMyPlan);
//        NFFT(vpr_complex)(lMyPlan.f, lMyPlan.M_total, "Data Fit, vector f = ");
//        NFFT_CSWAP(lMyPlanInverse.f_hat_iter, lMyPlan.f_hat);
    }

    pSolnFFT_real.resize(lMyPlan.N_total);
    pSolnFFT_imag.resize(lMyPlan.N_total);

    // Store The F_hat
    double lNormalize = lMyPlan.N[0] * lMyPlan.N[1];
    for ( int i = 0; i < lMyPlan.N_total; i++){
        pSolnFFT_real[i] = NFFT_M(creal)(lMyPlanInverse.f_hat_iter[i]);
        pSolnFFT_imag[i] = NFFT_M(cimag)(lMyPlanInverse.f_hat_iter[i]);
    }
    // Finalized NFFT
    SOLVER(finalize_complex)(&lMyPlanInverse);
    NFFT(finalize)(&lMyPlan);
    
    return lReturnStatus;
}

/** Writes the FFT of the solution in the specified file (in ASCII format)
 */
int NFFTCase::writeFFTSolutionFile_ASCII(){

    int lReturnStatus = 0;
    cout << " ----- Writing NFFT Solution (ASCII) : FFT Field ..." << endl;

    ofstream lFFTSolution;
    lFFTSolution.open(this->aFFTFile.c_str());

    // Write ASCII Header
    lFFTSolution << "# vtk DataFile Version 2.0" << endl;
    lFFTSolution << "NFFT Data produed by XainexNFFT3D V0.1" << endl;
    lFFTSolution << "ASCII" << endl;
    lFFTSolution << "DATASET STRUCTURED_GRID" << endl;
    lFFTSolution << "DIMENSIONS ";
    for ( int id = 0; id < this->aDimension; id++)
        lFFTSolution << this->aNoFFTModes[id] << " ";
    for ( int id = this->aDimension; id < 3; id++)
        lFFTSolution << "1 ";
    lFFTSolution << endl;

    // Write Points Coordinates
    int lNoNodes_total = 1;
    for ( int id = 0; id < this->aDimension; id++ )
        lNoNodes_total *= this->aNoFFTModes[id];

    lFFTSolution << "POINTS " << lNoNodes_total << " float" << endl;
    for ( int i = 0; i < lNoNodes_total; i++ ){
        // Decipher the nodes indx in each dimension
        int d = this->aDimension-1;
        vector<int> lNodeIndx(d+1,0);
        int Indx = i;
        for ( int id = d; id > 0; id--){
            lNodeIndx[id] = Indx % this->aNoFFTModes[id];
            Indx = (Indx - lNodeIndx[id]) / this->aNoFFTModes[id];
        }
        lNodeIndx[0] = Indx;
        for (int id = 0; id < this->aDimension; id++){
            double lNodeIndxFloat = double(lNodeIndx[id]);
            lFFTSolution << lNodeIndxFloat << " ";
        }
        for ( int id = this->aDimension; id < 3; id++)
            lFFTSolution << "0 ";
        lFFTSolution << endl;
    }

    lFFTSolution << "POINT_DATA " << lNoNodes_total << endl;
    // Write f_hat for each field
    for ( int ifield = 0; ifield < this->aNoFields4NFFT; ifield++ ){
        lFFTSolution << "SCALARS f_hat_real_" << ifield << " float" << endl;
        lFFTSolution << "LOOKUP_TABLE default" << ifield << endl;
        for ( int i = 0; i < lNoNodes_total; i++)
            lFFTSolution << this->aSolnFFT_real[ifield][i] << " ";
        lFFTSolution << endl;

        lFFTSolution << "SCALARS f_hat_imag_" << ifield << " float" << endl;
        lFFTSolution << "LOOKUP_TABLE default" << ifield << endl;
        for ( int i = 0; i < lNoNodes_total; i++)
            lFFTSolution << this->aSolnFFT_imag[ifield][i] << " ";
        lFFTSolution << endl;

        lFFTSolution << "SCALARS f_hat_" << ifield << " float" << endl;
        lFFTSolution << "LOOKUP_TABLE default" << ifield << endl;
        for ( int i = 0; i < lNoNodes_total; i++){
            double lMagnitude = sqrt( this->aSolnFFT_real[ifield][i] * this->aSolnFFT_real[ifield][i] +
                                      this->aSolnFFT_imag[ifield][i] * this->aSolnFFT_imag[ifield][i] );
            lFFTSolution << lMagnitude << " ";
        }
        lFFTSolution << endl;       
    }
    
    lFFTSolution.close();

    cout << " ----- Writing NFFT Solution (ASCII) : FFT Field ... Successfull!" << endl;
    return lReturnStatus;

}

/** Writes the FFT of the solution in the specified file
 */
int NFFTCase::writeFFTSolutionFile(){

    int lReturnStatus = 0;
    cout << " ----- Writing NFFT Solution : FFT Field ..." << endl;

    ofstream lFFTSolution(this->aFFTFile.c_str(), ofstream::binary);

    // Definition of the VTK file
    lFFTSolution << "<?xml version=\"1.0\"?>" << endl;
    lFFTSolution << "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">" << endl;

    // Definition of the structured grid
    lFFTSolution << "<StructuredGrid WholeExtent=\"";
    for ( int id = 0; id < this->aDimension; id++)
        lFFTSolution << 0 << " " << double(this->aNoFFTModes[id]-1) << " ";
    lFFTSolution << "\">" << endl;

    // Defining a new piece
    lFFTSolution << "<Piece Extent=\"";
    for ( int id = 0; id < this->aDimension; id++)
        lFFTSolution << 0 << " " << double(this->aNoFFTModes[id]-1) << " ";
    lFFTSolution << "\">" << endl;

    int lNoNodes_total = 1;
    for ( int id = 0; id < this->aDimension; id++ )
        lNoNodes_total *= this->aNoFFTModes[id];

    // Defining the point data (where the actual data is stored)
    // -----------------------
    int lOffset = 0;
    lFFTSolution << "<PointData Scalars=\"F_hat_0_mag\" Vectors=\"F_hat_0\">" << endl;
    for ( int i = 0; i < this->aNoFields4NFFT; i++){
       lFFTSolution << "<DataArray type=\"Float64\" Name=\"F_hat_" << i <<"\" NumberOfComponents=\"" << this->aDimension << "\" format=\"appended\" encoding=\"raw\" offset=\"" << lOffset <<"\"/>" << endl;
       lOffset += sizeof (int) + this->aDimension * lNoNodes_total * sizeof (double);
       lFFTSolution << "<DataArray type=\"Float64\" Name=\"F_hat_" << i << "_mag\" format=\"appended\" encoding=\"raw\" offset=\"" << lOffset <<"\"/>" << endl;
       lOffset += sizeof (int) + lNoNodes_total * sizeof (double);
    }
    lFFTSolution << "</PointData>" << endl;

    // Defining the cell data (empty)
    // -----------------------
    lFFTSolution << "<CellData></CellData>" << endl;

    // Defining the Points
    // -----------------------
    lFFTSolution << "<Points>" << endl;
    lFFTSolution << "<DataArray type=\"Float64\" NumberOfComponents=\"" << this->aDimension << "\" format=\"appended\" encoding=\"raw\" offset=\"" << lOffset << "\"/>" << endl;
    lOffset += sizeof(int) + lNoNodes_total * this->aDimension * sizeof (double);
    lFFTSolution << "</Points>" << endl;

    // Closing the piece
    lFFTSolution << "</Piece>" << endl;

    // Closing the structured grid
    lFFTSolution << "</StructuredGrid>" << endl;

    // Append the real data
    lFFTSolution << "<AppendedData encoding=\"raw\">\n";
    lFFTSolution << "_";

    // for each specified field
    for ( int ifield = 0; ifield < this->aNoFields4NFFT; ifield++){

        // Write the f_hat vector
        int lSize = this->aDimension * lNoNodes_total * sizeof (double);
        lFFTSolution.write((char*) &lSize, sizeof (int));
        for ( int in = 0; in < lNoNodes_total; in++ ){
            double lF_hat_real = this->aSolnFFT_real[ifield][in];
            double lF_hat_imag = this->aSolnFFT_imag[ifield][in]; 
            lFFTSolution.write(reinterpret_cast<char*> (&lF_hat_real), sizeof (double));
            lFFTSolution.write(reinterpret_cast<char*> (&lF_hat_imag), sizeof (double));
        }

        // Write the f_hat_mag
        lSize = lNoNodes_total * sizeof (double);
        lFFTSolution.write((char*) &lSize, sizeof (int));
        for ( int in = 0; in < lNoNodes_total; in++ ){
            double lF_hat_real = this->aSolnFFT_real[ifield][in];
            double lF_hat_imag = this->aSolnFFT_imag[ifield][in]; 
            double lF_hat_mag = sqrt( lF_hat_real * lF_hat_real + lF_hat_imag * lF_hat_imag);
            lFFTSolution.write(reinterpret_cast<char*> (&lF_hat_mag), sizeof (double));
        }

    }

    // Write the points coordinates
    int lSize = this->aDimension * lNoNodes_total * sizeof (double);
    lFFTSolution.write((char*) &lSize, sizeof (int));
    for ( int in = 0; in < lNoNodes_total; in++ ){
        // Decipher the nodes indx in each dimension
        int d = this->aDimension-1;
        vector<int> lNodeIndx(d+1,0);
        int Indx = in;
        for ( int id = d; id > 0; id--){
            lNodeIndx[id] = Indx % this->aNoFFTModes[id];
            Indx = (Indx - lNodeIndx[id]) / this->aNoFFTModes[id];
        }
        lNodeIndx[0] = Indx;
        for (int id = 0; id < this->aDimension; id++){
            double lNodeIndxFloat = double(lNodeIndx[id]);
            lFFTSolution.write((char*) &lNodeIndxFloat, sizeof (double));
        }
    }

    lFFTSolution << "\n";
    lFFTSolution << "</AppendedData>" << endl;

    // Closing the VTK file
    lFFTSolution << "</VTKFile>" << endl;

    lFFTSolution.close();

    cout << " ----- Writing NFFT Solution : FFT Field ... Successfull!" << endl;
    return lReturnStatus;
}
