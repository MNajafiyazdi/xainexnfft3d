#ifndef _PARSER_H
#define	_PARSER_H
#include "../../standardlibs.h"

string trim2(string str);

void die(string msg);

string parser_read_string(istream& pConfigFile, string pKeyWord, string pDefaultValue);
double parser_read_double(istream& pConfigFile, string pKeyWord, double pDefaultValue);
int parser_read_int(istream& pConfigFile, string pKeyWord, int pDefaultValue);

void parser_array_int(istream& pConfigFile, string pKeyWord, vector<int>& pVals);
void parser_array_int(string pString, vector<int>& pIntArray);


void parser_array_double(string pString, vector<double>& pIntArray);

void parser_array_string(istream& pConfigFile, string pKeyWord, vector<string>& pVals);
void parser_array_string(string pString, vector<string>& pStringArray);

template<typename T>
string convert_to_string(T pInput){
    ostringstream lString;
    lString << pInput;
    return lString.str();
}


#endif	/* _PARSER_H */
