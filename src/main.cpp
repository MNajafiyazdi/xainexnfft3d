/*
 * Copyright (c) 2016 Mostafa Najafiyazdi 
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define NFFT_PRECISION_DOUBLE

#include "nfft3mp.h"
#include "libs/case/NFFTCase.h"

/** Calculate the Non-uniform Fast Fourier Transform of a given solution
 *  on an unstructured grid. The NFFT is performed using the NFFT and FFTW3
 *  packages (see <a href="https://www-user.tu-chemnitz.de/~potts/nfft/index.php">here</a>.
 *  This program can be called in two modes:
 *  - <b>Forward Transform</b>: Given a set of complex coefficients, f_hat, 
 *  	in the Fourier space, the signal is constructed in the time/space domain.
 *  - <b>Backward Transform</b>: Given a set of sampling points from a signal
 *  	in the time/space domain, the complex coefficients in the Fourier space,
 *  	f_hat, are calculated.
 *
 *  Note that using NFFT package, the transforms are approximate and have 
 *  both aliasing and truncation errors.
 *
 *  \note{ In NFFT and FFTW3 packages, the forward transform 
 *  is the one which takes Fourier coefficients in the Fourier space, f_hat, 
 *  and outputs the signal in the physical time/space domain.} 
 */
int main(int argc, char** argv){

	int lExitStatus = 0;

	// Starting the NFFT to calculate the Discrete Fourier Transform
	// coefficients, f_hat,
	
	cout << " ==================================================" << endl;
	cout << "                 Xainex NFFT 3D " << endl;
	cout << " ==================================================" << endl;
    cout << " Copyright (c) 2016 by Mostafa Najafiyazdi." << endl;
    cout << " --------------------------------------------------" << endl;

	// Create an NFFT Case
	NFFTCase lMyNFFTCase;

    // Get the config file name from input arguments
    if ( argc > 1 ){
        lMyNFFTCase.aConfigFileName = argv[1];
    }

	// Read configure file
	int lErrorStatus = lMyNFFTCase.readConfigFile();
	if ( lErrorStatus ) return lErrorStatus;

	// Read solution file in CSV format with header
    lErrorStatus = lMyNFFTCase.readSolutionFile_CSV();	
    if ( lErrorStatus ) return lErrorStatus;

    // Apply Windowing if specified
    if ( lMyNFFTCase.aWindowFunction.length() > 0 ){
        lErrorStatus = lMyNFFTCase.doWindowOnFields();
        if ( lErrorStatus ) return lErrorStatus;
    }

	// Perform NFFT on the given set of fields of the read solution
    lErrorStatus = lMyNFFTCase.doNFFTonFields();
    if ( lErrorStatus ) return lErrorStatus;

	// Write solution in VTS format (for Paraview)
    lMyNFFTCase.writeFFTSolutionFile_ASCII();

	cout << " ==================================================" << endl;
	cout << "                 Finished !!!   " << endl;
	cout << " ==================================================" << endl;

	return lExitStatus;
}
