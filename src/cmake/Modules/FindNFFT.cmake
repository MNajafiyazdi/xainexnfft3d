#  Copyright Olivier Parcollet 2014.
#  Distributed under the Boost Software License, Version 1.0.
#      (See accompanying file LICENSE_1_0.txt or copy at
#          http://www.boost.org/LICENSE_1_0.txt)

#
# This module looks for nfft.
# It sets up : NFFT_INCLUDE_DIR, NFFT_LIBRARIES, and NFFT_FOUND
# 

SET(TRIAL_PATHS
 $ENV{NFFT_DIR}/include
 ${NFFT_DIR}/include
 /usr/include
 /usr/local/include
 /opt/local/include
 /sw/include
 )
FIND_PATH(NFFT_INCLUDE_DIR nfft3mp.h ${TRIAL_PATHS} DOC "Include for NFFT")

SET(TRIAL_LIBRARY_PATHS
 /usr/lib 
 /usr/local/lib
 /opt/local/lib
 /sw/lib
 $ENV{NFFT_DIR}/lib
 ${NFFT_DIR}/lib
 )

SET(NFFT_LIBRARIES "NFFT_LIBRARIES-NOTFOUND" CACHE STRING "NFFT library")
# Try to detect the lib
FIND_LIBRARY(NFFT_LIBRARIES nfft3 ${TRIAL_LIBRARY_PATHS} DOC "NFFT library")

mark_as_advanced(NFFT_LIBRARIES)
mark_as_advanced(NFFT_INCLUDE_DIR)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(NFFT DEFAULT_MSG NFFT_LIBRARIES NFFT_INCLUDE_DIR)
