#ifndef STANDARDLIBS_H
#define	STANDARDLIBS_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <math.h>
#include <ctime>
#include <fstream>   // file I/O
#include <iomanip>   // format manipulation
#include <time.h>
#include <cstdlib>
#include <vector>
#include <map>

using namespace std;

enum {
	Forward_NFFT, Backward_NFFT
};

#endif	/* STANDARDLIBS_H */
