XainexNFFT3D V0.1
==================================
Introduction
------------
XainexNFFT3D is a post processing package which calculates 1D, 2D, or 3D Fourier transform on an unstructured data, It is based on the [Non-Uniform FFT (NFFT)](https://www-user.tu-chemnitz.de/~potts/nfft/) package by J. Keiner, S. Kunis, and D. Potts from Chemnitz University of Technology, Department of Mathematics.

The input is a CSV file with headers like written as *"Header 1"* (with double quotations). It must include the location, i.e. (x,y,z), of data points in columns named *"Position:Y"* where *Y* can be *0*, *1*, or *2*.

The output is a VTS file written for [ParaView](http://www.paraview.org/) (a post processing OpenSource package).
