#!/bin/bash

#PBS -l nodes=1:ppn=24
#PBS -q qwork
#PBS -A inn-251-af
#PBS -l walltime=0:10:00:00
#PBS -o qsub_output.log
#PBS -e qsub_error.log
#PBS -m bea
#PBS -M mostafa.najafiyazdi@mail.mcgill.ca
#PBS -V
#PBS -N NFFT_Gaussian3D

cd ~/codes/XainexNFFT3D/test/validation/3D/02_Gaussian
./XainexNFFT3D_V0.1 config.nfft 2>&1 | tee nfft.log
